with Ada.Text_IO;

use Ada.Text_IO;

procedure RemoveDoubles(filein,fileout: String) is
	Fin,Fout: File_Type;
	CurChar,LastChar: Character;
begin
	Open(Fin,In_File,filein);
	Open(Fout,Out_File,fileout);

	Get(Fin,LastChar);
	while not End_Of_File(Fin) loop
		Get(Fin,CurChar);
		if CurChar/=LastChar then
			Put(Fout,LastChar);
			LastChar:=CurChar;
		end if;
	end loop;
	Put(Fout,LastChar);
	Close(Fin);
	Close(Fout);
end;
