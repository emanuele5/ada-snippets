with Definitions;

use Definitions;

function Average(arr: FloatArray) return Float is
	Result: Float :=0.0;
begin
	for i in arr'Range loop
		Result:=Result+arr(i);
	end loop;
	return Result/Float(arr'Length);
end;
