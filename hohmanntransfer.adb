with Ada.Numerics.Elementary_Functions;

use Ada.Numerics;
use Ada.Numerics.Elementary_Functions;

procedure HohmannTransfer(StartRadius, EndRadius, GravParam: Float; DV1, DV2, TransferTime: out Float) is
	RSum: Float;
begin
	RSum:=StartRadius+EndRadius;
	DV1:=abs( Sqrt(GravParam/StartRadius) * (Sqrt(2.0*EndRadius/Rsum)-1.0) );
	DV2:=abs( Sqrt(GravParam/EndRadius) * (1.0-Sqrt(2.0*StartRadius/RSum)) );
	TransferTime:=Pi*Sqrt((RSum**3)/(8.0*GravParam));
end HohmannTransfer;
