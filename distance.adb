with Ada.Numerics.Elementary_Functions;
with Definitions;

use Ada.Numerics.Elementary_Functions;
use Definitions;

function Distance(a,b: Point) return Float is
begin
	return sqrt((a.x-b.x)**2+(a.y-b.y)**2);
end Distance;
