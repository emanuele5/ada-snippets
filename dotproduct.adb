with Definitions;

use Definitions;

function DotProduct(x,y: FloatArray) return Float is
	Result: Float :=0.0;
begin
	if x'First/=y'First or y'Last/=x'Last then
		return 0.0;
	end if;
	for i in x'Range loop
		Result:=Result+x(i)*y(i);
	end loop;
	return Result;
end;
