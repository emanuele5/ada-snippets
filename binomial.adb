function Binomial(n,k: Natural) return Float is
	nfatt,kfatt,nkfatt: Float :=1.0;
begin
	if n=0 or k>n then
		return -1.0;
	end if;
	for i in 2..n loop
		nfatt:=nfatt*Float(i);
		if i=k then
			kfatt:=nfatt;
		end if;
		if i=n-k then
			nkfatt:=nfatt;
		end if;
	end loop;
	return nfatt/(kfatt*nkfatt);
end Binomial;
