function SymmetricBinomial(n,h,k: Natural) return Float is

	function Factorial(Number: Natural) return Float is
		Result: Float:=1.0;
	begin
		for i in 1..Number loop
			Result:=Result*Float(i);
		end loop;
		return Result;
	end Factorial;

begin
	return Factorial(n)/(Factorial(h)*Factorial(k));
end SymmetricBinomial;
