with Ada.Numerics.Elementary_Functions;
with Definitions;

use Ada.Numerics.Elementary_Functions;
use Definitions;

procedure Rotation(p: in out Point; alpha: Float) is
	Result: Point;
begin
	Result.x:=p.x*cos(alpha)+p.y*sin(alpha);
	Result.y:=p.x*sin(alpha)+p.y*cos(alpha);
	p:=Result;
end Rotation;
