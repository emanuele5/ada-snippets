function CountSpaces(s: String) return Natural is
	n: Natural :=0;
begin
	for i in s'Range loop
		if s(i)=' ' then
			n:=n+1;
		end if;
	end loop;
	return n;
end CountSpaces;
