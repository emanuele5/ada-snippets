package Definitions is

	type FloatArray is array(Integer range <>) of Float;

	type Point is record
		x,y: Float;
	end record;

end Definitions;
