with Ada.Text_IO;

use Ada.Text_IO;

function FindMaximum(filename: String) return Integer is

	package IntIO is new Integer_IO(Integer);

	F: File_Type;
	Num: Integer;
	Max: Integer :=Integer'First;
begin
	Open(F,In_File,filename);
	while not End_Of_File(F) loop
		IntIO.Get(F,Num);
		if Num>Max then
			Max:=Num;
		end if;
	end loop;
	Close(F);
	return Max;
end FindMaximum;
